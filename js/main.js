function eventClick(el, func) {
    if (el) {
        el.addEventListener('click', func);
    }
}

window.onload = function () {


    /* Show / hide password(entrance)*/
    let hide = document.querySelector('.hide-password'),
        open = document.querySelector('.open-password');

    eventClick(hide, function () {
        hide.classList.toggle('hide-password')
        hide.classList.toggle('open-password-active')
    });

    /* Show / hide password (registration) */
    let regHide = document.querySelector('.registration-hide'),
        regOpen = document.querySelector('.registration-open');

    eventClick(regHide, function () {
        regHide.classList.toggle('registration-hide')
        regHide.classList.toggle('registration-active')
    });

    /* select in registration */

    var x, i, j, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /* For each element, create a new DIV that will act as the selected item: */
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected", "group-date");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /* For each element, create a new DIV that will contain the option list: */
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
            /* For each option in the original select element,
            create a new DIV that will act as an option item: */
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /* When an item is clicked, update the original select box,
                and the selected item: */
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /* When the select box is clicked, close any other select boxes,
            and open/close the current select box: */
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }

    function closeAllSelect(elmnt) {
        /* A function that will close all select boxes in the document,
        except the current select box: */
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items", "group-items");
        y = document.getElementsByClassName("select-selected", "group-date");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }

    /* If the user clicks anywhere outside the select box,
    then close all select boxes: */
    document.addEventListener("click", closeAllSelect);





    /* Firuga in the Tab window of registration and entry */

    let ent = document.querySelector('.decor-figure'),
        reg = document.querySelector('.decor-figure4'),
        figuteActive = document.querySelector('.decor-figure__active'),
        tabEnt = document.querySelector('.tab1'),
        tabReg = document.querySelector('.tab2');

    eventClick(tabEnt, function () {
        reg.classList.remove('decor-figure__active')
        ent.classList.add('decor-figure__active')
    });

    eventClick(tabReg, function () {
        ent.classList.remove('decor-figure__active')
        reg.classList.add('decor-figure__active')
    });



    /* Переключение tab вкладок */
    let tabEnter = document.getElementById('tab-box1'),
        tabRegist = document.getElementById('tab-box2');

    eventClick(tabRegist, function () {
        tabRegist.classList.add('tab-active');
        tabEnter.classList.remove('tab-active');
    });

    eventClick(tabEnter, function () {
        tabRegist.classList.remove('tab-active');
        tabEnter.classList.add('tab-active');
    });


    /* modile menu */
    let burger = document.querySelector('.burger'),
        mobileMenu = document.querySelector('.modile-menu'),
        mobileClosed = document.querySelector('.modile-menu__closed');

    eventClick(burger, function () {
        mobileMenu.classList.add('modile-active');
    });

    eventClick(mobileClosed, function () {
        mobileMenu.classList.remove('modile-active');
    });


    //nujno vse dodelat 

    /* Show / hide password (cabinet) */
    let subscribe = document.querySelector('.property-block__btn'),
        unsubscribe = document.querySelector('.property-block__repeal');


    eventClick(subscribe, function () {
        subscribe.classList.toggle('property-active');
        unsubscribe.classList.toggle('property-active');
    });

    eventClick(unsubscribe, function () {
        unsubscribe.classList.toggle('property-active');
        subscribe.classList.toggle('property-active');
    });



    /* fix the menu in the container */
    (function () {
        var a = document.querySelector('#aside1'), b = null, P = 0;
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);
        function Ascroll() {
            if (b == null) {
                var Sa = getComputedStyle(a, ''), s = '';
                for (var i = 0; i < Sa.length; i++) {
                    if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                        s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                    }
                }
                b = document.createElement('div');
                b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                a.insertBefore(b, a.firstChild);
                var l = a.childNodes.length;
                for (var i = 1; i < l; i++) {
                    b.appendChild(a.childNodes[1]);
                }
                a.style.height = b.getBoundingClientRect().height + 'px';
                a.style.padding = '0';
                a.style.border = '0';
            }
            var Ra = a.getBoundingClientRect(),
                R = Math.round(Ra.top + b.getBoundingClientRect().height - document.querySelector('#article').getBoundingClientRect().bottom);  // селектор блока, при достижении нижнего края которого нужно открепить прилипающий элемент
            if ((Ra.top - P) <= 0) {
                if ((Ra.top - P) <= R) {
                    b.className = 'stop';
                    b.style.top = - R + '50px';
                } else {
                    b.className = 'sticky';
                    b.style.top = P + '50px';
                }
            } else {
                b.className = '';
                b.style.top = '';
            }
            window.addEventListener('resize', function () {
                a.children[0].style.width = getComputedStyle(a, '').width
            }, false);
        }
    })()
};

function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (o.scrollHeight) + "px";
}




// Выбор только одного чек бокса
inputs = document.querySelectorAll(".checkbox1");
for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == "checkbox") {
        inputs[i].onchange = function () {
            inputs = document.querySelectorAll(".checkbox1");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    inputs[i].checked = false;
                }
                this.checked = true;
            }
        }
    }
};

inputs = document.querySelectorAll(".checkbox2");
for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == "checkbox") {
        inputs[i].onchange = function () {
            inputs = document.querySelectorAll(".checkbox2");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    inputs[i].checked = false;
                }
                this.checked = true;
            }
        }
    }
};

inputs = document.querySelectorAll(".checkbox3");
for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == "checkbox") {
        inputs[i].onchange = function () {
            inputs = document.querySelectorAll(".checkbox3");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    inputs[i].checked = false;
                }
                this.checked = true;
            }
        }
    }
};

inputs = document.querySelectorAll(".checkbox4");
for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].type == "checkbox") {
        inputs[i].onchange = function () {
            inputs = document.querySelectorAll(".checkbox4");
            for (var i = 0; i < inputs.length; i++) {
                if (inputs[i].type == "checkbox") {
                    inputs[i].checked = false;
                }
                this.checked = true;
            }
        }
    }
};